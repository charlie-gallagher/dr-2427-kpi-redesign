# KPI_01_query.R
# Author: Charlie Gallagher (charlie@ixisdigital.com)
# Description: Query the Adobe database for the data needed to make visuals
# Date: 26 April 2021

cli::cli_alert_info("...Starting: KPI_01_query.R")


# Ixpress connection ----

## Metadata ----
metadata <- list('dims' = filter(aa_dims, ID == "Day"),
                 'mets' = filter(aa_mets, ID == "UniqueVisitors"))


## Create the ixpressConn ----
aa_conn_audi <- ixpressConn(
    connection_ID = GLOBALS$RSID_AUDI,
    query_fun = "aa_basic",
    auth_fun = "auth_aa_basic",
    auto_auth = FALSE,
    metrics_metadata = metadata$mets,
    dimensions_metadata = metadata$dims
  )


# Ixpress Query ----
ixtra::message_info(">>> STARTING QUERY <<<")

## Supplemental data ----

# Metric, dates
mets_aa <- c("UniqueVisitors")

kpi_segs <-
    tribble(
        ~segment_name,                    ~abbreviation,
        "_Tier 1 (Hit)",                  "t1",
        "_MLP View (Hit)",                "t1MLPView",
        "_T1 > T3 (Visit)",                 "t1toT3",
        "_RAQ Start (Hit)",               "t1RAQStart",
        "_RAQ Submit Success (Hit)",       "t1RAQSubmit",
        "_Configurator (Hit)",            "t1Configurator",
        "_Configurator Complete (Hit)",   "t1ConfiguratorComplete",
        "_Inventory Results Page (Hit)",  "t1IRP",
        "_Tier 2 (Hit)",                  "t2",
        "_Tier 2 Lead Submission (Hit)",  "t2Lead",
        "_T2 VDP (Hit)",                  "t2VDP",
        "_T2 VLP (Hit)",                  "t2VLP",
        "_Tier 3 (Hit)",                  "t3",
        "_Tier 3 Lead Submission (Hit)",  "t3Lead",
        "_Tier 3 VDP (Hit)",              "t3VDP",
        "_T3 VLP (Hit)",                  "t3VLP",
    ) %>% 
  pull(segment_name)



SCAuth(Sys.getenv("RSITECATALYST_KEY"), Sys.getenv("RSITECATALYST_SECRET"))
segs_audi <- get_segments_insistently(GLOBALS$RSID_AUDI) %>% as_tibble()
id_segs_audi <- get_segment_id2(kpi_segs, segs_audi)


## Build query ----
aa_query <- aa_conn_audi %>%
  ixpressQuery() %>%
  DURING(GLOBALS$QUERY_DATES) %>%
  SELECT(mets_aa) %>%
  GROUP_BY("Day") %>%
  SEGMENT_BY(id_segs_audi)

## Run query ----
aa_data <- aa_query %>%
  COLLECT()
