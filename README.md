# Weekly Email KPI Plots
Every week in the weekly email, Key Performance Indicator (KPI) plots are
included to inform readers at a glance what's changing week to week. This folder
contains a new KPI plot written using `ixpress` instead of `IXIS.Auto`. 

### Scope
The KPI plots should stimulate interest, but you should avoid giving too much
analysis. At the foot of the plots is a link to a weekly dashboard and the IAR.
For more information, readers should be directed there, or to a spotlight. 

The plots contain data on important metrics covering most recent 8 weeks, with
Monday as the first day of the week. The graphs also show a shadow line (light
grey) for the same 8 weeks the previous year. At the moment, this is done at the
week level (last year's 8 weeks are defined as exactly 52 weeks prior to this
year's 52 weeks). This may change in the future. 

### KPIs Included
Nine KPIs may be included each week, from a pool of 16 I think. Several are
constant from week to week, while others rotate in and out. 


### Prerequisites
You will need the following packages: 

- tidyverse
- ixpress
- ixtra
- lubridate
- RSiteCatalyst
- ragg
- systemfonts
- cowplot
- glue

In addition to these, you will need the Audi fonts available for use with `ragg`
and `systemfonts`. The fonts should be installed on your system in such a way
that `systemfonts` will be able to find them. Once that's done, there's one more
step: registering the font faces. See, systemfonts doesn't do a good job
distinguishing between bold and plain face styles, so you need to register your
own fonts. I wrote a script for this that runs automatically, assuming again
that `systemfonts` can find the fonts. 

Note that if you are running **Windows** you will have to modify the font
register script so that file path contains escaped backslashes. 

`audi_adobe_utils.R` contains utility functions I do (and don't) use in this
project. Nothing special. 

In your R Environment (usually things included in your `.Renviron` file in your
home directory), you need to have a key and secret key for `RSiteCatalyst`. 

Finally, you will need to have `ixpress` and `ixtra` installed and available.
These are internal to IXIS, so if you have any trouble you should reach out to
someone about installing packages from GitLab instead of CRAN. 

As long as these requirements are met, you will be able to simply source the
`exec` file and be greeted by a new KPI graphic. 


### Files

- `KPI_00_exec.R` Load packages, source utility files, 
- `KPI_01_query.R` Query the Adobe Analytics database for the data. This file
  should only contain querying---anything else is out of scope. 
- `KPI_02_processing.R` Taking the raw-ish data and reducing it to only what is
  necessary for plotting. This file should contain anything that limits or
affects the raw data. 
- `KPI_03_visualize.R` This is the visualization script. Any modifications to
  the data that are exclusively used for visuals should go in this file. That
includes additional variables, pivoting the data, making new datasets for
plotting purposes, and so on.
- `audi_adobe_utils.R` and `register_audi_fonts.R`
As explained above, these are utilities you need for preparing the query and
making fonts available. You should not need to change these, except if you are
on Windows, in which case you'll need to modify `register_audi_fonts.R`. 


---

Charlie Gallagher, May 2021
